module.exports = function (app) {
    var MongoDB = app.dataSources.MongoDB;
    var Promise = require('bluebird');

    MongoDB.autoupdate('Customer', function (err) {
        if (err) throw (err);
        var Customer = app.models.Customer;
        var Role = app.models.Role;


            Role.find().then(function (results) {
                if(results.length == 0){
                    Customer.create([
                        {
                            username: 'Admin',
                            email: 'admin@financetracker.com',
                            password: 'password'
                    },
                        {
                            username: 'hello',
                            email: 'hello@financetracker.com',
                            password: 'password'
                        }
          ], function (err, users) {
                        if (err) throw (err);
                        var Role = app.models.Role;
                        var RoleMapping = app.models.RoleMapping;
        
                        //create the admin role
                        Role.create({
                            name: 'admin'
                        }, function (err, role) {
                            if (err) throw (err);
                            //make admin
                            role.principals.create({
                                principalType: RoleMapping.USER,
                                principalId: users[0].id
                            }, function (err, principal) {
                                if (err) throw (err);
                            });
                        });
                    });
                }
            })
            .catch(function (err) {});
    });

};